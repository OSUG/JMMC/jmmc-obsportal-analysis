#!/usr/bin/env python

# coding=utf-8
from io import open

import re
import csv
import arrow
import click
import os
import pyvo as vo

from capturer import CaptureOutput

from astropy import units as u
from astropy.coordinates import Angle
from astropy.io import ascii
from astropy.time import Time

from astroquery.eso import Eso

from lxml import etree

import requests
from bs4 import BeautifulSoup

from datetime import timedelta


## INSTRUMENTS
########################################################################################################################

instruments = [
    'midi',
    'amber',
    'pionier',
    'gravity',
    'matisse',
]


## COMMAND LINE ENTRY POINT
########################################################################################################################

def _makedirs(the_path):
    try:
        os.makedirs(the_path)
    except:
        # Do not raise an exception when the path already exists
        pass


@click.group()
def cli():
    # Make output directory for headers
    _makedirs(os.path.join(os.path.abspath('.'), 'outputs', 'headers'))

    # Make output directories for all instruments
    for instrument in instruments:
        _makedirs(os.path.join(os.path.abspath('.'), 'outputs', instrument))


## EXPORT INSTRUMENT HELP PAGES FROM ESO ARCHIVE
########################################################################################################################

@cli.command()
def eso_help_instruments():
    eso = Eso()

    for instrument in instruments:
        with CaptureOutput() as capturer:
            eso.query_instrument(instrument, help=True)
            with open(os.path.join(os.path.abspath('.'), 'outputs', instrument, 'help.txt'), 'wb') as output:
                output.writelines(capturer.get_bytes())


## EXPORT INSTRUMENT QUERIES AND HEADERS FROM ESO ARCHIVE
########################################################################################################################

@cli.command()
def eso_query_instruments():
    eso = Eso()

    for instrument in instruments:
        query = eso.query_instrument(instrument)
        ascii.write(query.columns, os.path.join(os.path.abspath('.'), 'outputs', instrument, 'query.txt'), names=query.colnames)

        header = eso.get_headers(query['DP.ID'][:1])
        ascii.write(header.columns, os.path.join(os.path.abspath('.'), 'outputs', instrument, 'header.txt'), names=header.colnames)

        with open(os.path.join(os.path.abspath('.'), 'outputs', instrument, 'query_colnames.txt'), 'w') as output:
            for colname in query.colnames:
                output.write(colname)
                output.write('\n')

        with open(os.path.join(os.path.abspath('.'), 'outputs', instrument, 'header_colnames.txt'), 'w') as output:
            for colname in header.colnames:
                output.write(colname)
                output.write('\n')



## COMPARE OIDB WITH ESO ARCHIVE
########################################################################################################################

#@cli.command()
#@click.argument('instrument', type=click.Choice(instruments, case_sensitive=False))
#@click.argument('obs_id')
#def compare_oidb(instrument, obs_id):
#
#    oidb = vo.dal.TAPService("http://oidb-beta.jmmc.fr/tap")
#    oidb_result = oidb.search("SELECT ALL * FROM oidb WHERE instrument_name = '%s' AND obs_id = '%s' " % (instrument.upper(), obs_id))
#
#    eso = Eso()
#    eso_result = eso.query_instrument(instrument, column_filters={'dp_id': obs_id})
#

## GENERATE AN ASPROX FILE FROM ESO ARCHIVE
########################################################################################################################

# See https://stackoverflow.com/questions/25132998/how-to-add-namespace-prefix-to-attribute-with-lxml-node-is-with-other-namespace

class AsproXMLNamespaces:
    a = 'http://www.jmmc.fr/aspro-oi/0.1'
    o = 'http://www.jmmc.fr/aspro-ob/0.1'
    rawobs = 'http://www.jmmc.fr/aspro-raw-obs/0.1'
    tm = 'http://www.jmmc.fr/jmcs/models/0.1'
    xsi = 'http://www.w3.org/2001/XMLSchema-instance'


asprox_namespaces = {
    'a': AsproXMLNamespaces.a,
    'o': AsproXMLNamespaces.o,
    'ro': AsproXMLNamespaces.rawobs,
    'tm': AsproXMLNamespaces.tm,
    'xsi': AsproXMLNamespaces.xsi
}


def _header_to_asprox(eso_header_row):

    # XML
    root = etree.Element(etree.QName(AsproXMLNamespaces.a, 'observationSetting'), nsmap=asprox_namespaces)

    etree.SubElement(root, 'schemaVersion').text = '2018.04'
    etree.SubElement(root, 'targetVersion').text = '2019.11'
    etree.SubElement(root, 'name').text = eso_header_row['ARCFILE'] #'default'

    # When
    when = etree.SubElement(root, 'when')
    etree.SubElement(when, 'date').text = arrow.get(eso_header_row['DATE-OBS']).format('YYYY-MM-DD') #'2017-09-28'

    # interferometerConfiguration
    interferometerConfiguration = etree.SubElement(root, 'interferometerConfiguration')
    etree.SubElement(interferometerConfiguration, 'name').text = 'VLTI Period 105'
    etree.SubElement(interferometerConfiguration, 'minElevation').text = '45.0'

    # instrumentConfiguration
    instrumentConfiguration = etree.SubElement(root, 'instrumentConfiguration')
    etree.SubElement(instrumentConfiguration, 'name').text = eso_header_row['INSTRUME']
    etree.SubElement(instrumentConfiguration, 'stations').text = eso_header_row['HIERARCH ESO OBS BASELINE'].replace('-', ' ') #'A0 G1 J2 K0'

    etree.SubElement(instrumentConfiguration, 'instrumentMode').text = 'HIGH-COMBINED'
    etree.SubElement(instrumentConfiguration, 'samplingPeriod').text = '20.0'
    etree.SubElement(instrumentConfiguration, 'acquisitionTime').text = '%s' % eso_header_row['EXPTIME'] #'30.0'

    # target (min 1, max n)
    target = etree.SubElement(root, 'target', id=eso_header_row['HIERARCH ESO OBS TARG NAME']) #'HD_16234'
    etree.SubElement(target, 'name').text = eso_header_row['HIERARCH ESO OBS TARG NAME'] #'HD 16234'
    etree.SubElement(target, 'RA').text = '%s' % Angle(eso_header_row['RA'] * u.deg / 15.0).to_string(sep=':') #'02:36:37.9182097570'
    etree.SubElement(target, 'DEC').text = '%s' % Angle(eso_header_row['DEC'] * u.deg).to_string(sep=':') #'+12:26:51.459132329'
    etree.SubElement(target, 'EQUINOX').text = '%s' % eso_header_row['EQUINOX'] #'2000.0'

    #configuration = etree.SubElement(target, 'configuration')
    #etree.SubElement(configuration, 'HAMin').text = '1.4'
    #etree.SubElement(configuration, 'HAMax').text = '12.0'
    #etree.SubElement(configuration, 'aoSetup').text = 'NAOMI_BRIGHT'
    #etree.SubElement(configuration, 'fringeTrackerMode').text = 'FringeTrack'

    # variant (optional)
    variant = etree.SubElement(root, 'variant')
    etree.SubElement(variant, 'stations').text = eso_header_row['HIERARCH ESO OBS BASELINE'].replace('-', ' ') #'A0 G1 J2 K0'

    #END
    return etree.tostring(root, xml_declaration=True, encoding='utf-8', standalone="yes", pretty_print=True)


@cli.command()
@click.argument('instrument')#, type=click.Choice(instruments, case_sensitive=False))
@click.argument('obs_id')
def eso_to_asprox(instrument, obs_id):

    eso = Eso()
    eso_result = eso.query_instrument(instrument, column_filters={'dp_id': obs_id})
    #print(eso_result)

    # Get header
    eso_header = eso.get_headers(eso_result['DP.ID'])

    # Build Asprox
    for eso_header_row in eso_header:
        with open(os.path.join(os.path.abspath('.'), 'outputs', instrument, '%s.asprox' % eso_header_row['ARCFILE']), 'wb') as output:
            asprox = _header_to_asprox(eso_header_row)
            output.write(asprox)


## GENERATE AN OBXML FILE FROM ESO ARCHIVE
########################################################################################################################

def _header_to_obxml(root, eso_header_row):

    if not eso_header_row['ARCFILE']:
        return

    cat = eso_header_row['HIERARCH ESO DPR CATG']
    #print(f'cat: {cat}')
#    print(eso_header_row)

    if not (cat in ['SCIENCE', 'CALIB']):
        return

    # create OB
    ob = etree.SubElement(root, 'observingBlockDefinition')
    etree.SubElement(ob, 'schemaVersion').text = '2017.7'
    etree.SubElement(ob, 'name').text = eso_header_row['ARCFILE']

    progId = eso_header_row['HIERARCH ESO OBS PROG ID']

    etree.SubElement(ob, 'programId').text = progId
    etree.SubElement(ob, 'observationId').text = '%s' % eso_header_row['HIERARCH ESO OBS ID']
    # ARCFILE / container id ?

    # interferometerConfiguration
    interferometerConfiguration = etree.SubElement(ob, 'interferometerConfiguration')
    etree.SubElement(interferometerConfiguration, 'name').text = 'VLTI'
    etree.SubElement(interferometerConfiguration, 'version').text = 'Period %s' % progId[1:4]
    etree.SubElement(interferometerConfiguration, 'stations').text = eso_header_row['HIERARCH ESO OBS BASELINE'].replace('-', ' ') #'A0 G1 J2 K0'

    # instrumentConfiguration
    instrument = eso_header_row['INSTRUME']
    instrumentConfiguration = etree.SubElement(ob, 'instrumentConfiguration')
    etree.SubElement(instrumentConfiguration, 'name').text = instrument

    # TODO: gather instrument mode mapping rules:
    if ('GRAVITY' == instrument):
        etree.SubElement(instrumentConfiguration, 'instrumentMode').text = '%s-%s' % (eso_header_row['HIERARCH ESO INS SPEC RES'], eso_header_row['HIERARCH ESO INS POLA MODE'])
    else:
        etree.SubElement(instrumentConfiguration, 'instrumentMode').text = 'UNDEFINED'

    # observationConfiguration
    observationConfiguration = etree.SubElement(ob, 'observationConfiguration')
    etree.SubElement(observationConfiguration, 'type').text = cat # TODO: fix CALIB = CALIBRATION

    # target (min 1, max n)
    target_name = eso_header_row['HIERARCH ESO OBS TARG NAME']
    target = etree.SubElement(observationConfiguration, 'SCTarget') # id is unique but targets may be repeated !
    etree.SubElement(target, 'name').text = target_name #'HD 16234'
    etree.SubElement(target, 'RA').text = '%s' % Angle(eso_header_row['RA'] * u.deg / 15.0).to_string(sep=':') #'02:36:37.9182097570'
    etree.SubElement(target, 'DEC').text = '%s' % Angle(eso_header_row['DEC'] * u.deg).to_string(sep=':') #'+12:26:51.459132329'
    etree.SubElement(target, 'EQUINOX').text = '%s' % eso_header_row['EQUINOX'] #'2000.0'

    # observationConstraints
    when = etree.SubElement(observationConfiguration, 'observationConstraints')
    # date / mjd (TEST new fields)
    etree.SubElement(when, 'date').text = arrow.get(eso_header_row['DATE-OBS']).format('YYYY-MM-DD') # '2017-09-28'
    etree.SubElement(when, 'mjd').text = '%s' % eso_header_row['MJD-OBS']
    lst_start = eso_header_row['LST']
    lst_end = lst_start + eso_header_row['EXPTIME']
    # todo convert in HH:MM:SS
    etree.SubElement(when, 'LSTinterval').text = '%s/%s' % (lst_start, lst_end) # 04:01/04:21


## GENERATE A RAW OBS FILE FROM ESO ARCHIVE
########################################################################################################################

def _header_to_rawobs_xml(root, eso_header_row):

    if not eso_header_row['ARCFILE']:
        return

    cat = eso_header_row['HIERARCH ESO DPR CATG']
    #print(f'cat: {cat}')
#    print(eso_header_row)

    if not (cat in ['SCIENCE', 'CALIB']):
        return

    # create OB
    ob = etree.SubElement(root, 'observation')
    etree.SubElement(ob, 'obsId').text = eso_header_row['ARCFILE']
    etree.SubElement(ob, 'type').text = cat # TODO: fix CALIB = CALIBRATION

    etree.SubElement(ob, 'parentId').text = '%s' % eso_header_row['HIERARCH ESO OBS ID'] # TODO: OB or container id

    progId = eso_header_row['HIERARCH ESO OBS PROG ID']
    etree.SubElement(ob, 'programId').text = progId

    # interferometer
    etree.SubElement(ob, 'interferometerName').text = 'VLTI'
    etree.SubElement(ob, 'interferometerVersion').text = 'Period %s' % progId[1:4]

    # array setup
    etree.SubElement(ob, 'stations').text = eso_header_row['HIERARCH ESO OBS BASELINE'].replace('-', ' ') #'A0 G1 J2 K0'

    # instrument setup
    instrument = eso_header_row['INSTRUME']
    etree.SubElement(ob, 'instrumentName').text = instrument

    # TODO: gather instrument mode mapping rules:
    if ('GRAVITY' == instrument):
        etree.SubElement(ob, 'instrumentMode').text = '%s-%s' % (eso_header_row['HIERARCH ESO INS SPEC RES'], eso_header_row['HIERARCH ESO INS POLA MODE'])
    else:
        etree.SubElement(ob, 'instrumentMode').text = 'UNDEFINED'

    etree.SubElement(ob, 'instrumentSubMode').text = 'SINGLE' # TODO: get SINGLE / DUAL

    # target
    target_name = eso_header_row['HIERARCH ESO OBS TARG NAME']
    etree.SubElement(ob, 'targetName').text = target_name #'HD 16234'
    etree.SubElement(ob, 'targetRa').text = '%s' % eso_header_row['RA']
    etree.SubElement(ob, 'targetDec').text = '%s' % eso_header_row['DEC']

    # exposure time
    mjd_start = eso_header_row['MJD-OBS']
    etree.SubElement(ob, 'mjdStart').text = '%s' % mjd_start

    mjd_end = mjd_start + eso_header_row['EXPTIME'] / (86400.0) # s to days
    etree.SubElement(ob, 'mjdEnd').text = '%s' % mjd_end

    etree.SubElement(ob, 'projectedBaselines').text = ''


## OBSERVED TARGET
########################################################################################################################

instrument_technique = {
    'midi': 'INTERFEROMETRY',
    'amber': 'INTERFEROMETRY',
    'pionier': 'INTERFEROMETRY',
    'gravity': 'INTERFEROMETRY',
    'matisse': 'INTERFEROMETRY',
}

def mjd_to_isot(mjd):
    t = Time(mjd, format='mjd')
    t.format = 'isot'
    return t.value

@cli.command()
@click.option('--instrument', type=click.Choice(instruments, case_sensitive=False))
@click.option('--target_name', default=None)
@click.option('--target_ra', default=None)
@click.option('--target_dec', default=None)
@click.option('--prog_id', default=None)
@click.option('--obxml', default=None)
@click.option('--rawobs', default=None)
def eso_observed_target(instrument, target_name, target_ra, target_dec, prog_id, obxml, rawobs):

    # Build the query
    query_filters = {
        #'dp_cat': 'SCIENCE',
        'dp_tech': 'INTERFEROMETRY',
    }

    if instrument:
        query_filters['instrument'] = instrument

    if target_name:
        query_filters['target'] = target_name

    if target_ra:
        query_filters['ra'] = target_ra

    if target_dec:
        query_filters['dec'] = target_dec

    if prog_id:
        query_filters['prog_id'] = prog_id

    if obxml:
        # XML
        root = etree.Element(etree.QName(AsproXMLNamespaces.o, 'observingBlockList'), nsmap=asprox_namespaces)

    if rawobs:
        # XML
        rootO = etree.Element(etree.QName(AsproXMLNamespaces.rawobs, 'targetObservations'), nsmap=asprox_namespaces)
        etree.SubElement(rootO, 'targetRef').text = 'TARGET_REF'

    # Execute the query
    eso = Eso()
    eso_result = eso.query_main(column_filters=query_filters)

    if eso_result:

        # Get headers for all results
        eso_headers = eso.get_headers(eso_result['Dataset ID'])

        # Build dictionnary
        for eso_header_row in eso_headers:
            if obxml:
                _header_to_obxml(root, eso_header_row)
            if rawobs:
                _header_to_rawobs_xml(rootO, eso_header_row)

            row_data = {
                'instrument': eso_header_row['INSTRUME'],
                'prog_id': eso_header_row['HIERARCH ESO OBS PROG ID'],
                'target': eso_header_row['HIERARCH ESO OBS TARG NAME'],
                'category': eso_header_row['HIERARCH ESO DPR CATG'],

                'exposure': eso_header_row['EXPTIME'],
                'lst': eso_header_row['LST'],
                'utc': eso_header_row['UTC'],
                'mjd_min': eso_header_row['MJD-OBS'],
                'isot_min': mjd_to_isot(eso_header_row['MJD-OBS']),
            }

            print(row_data)

        if obxml:
            obx = etree.tostring(root, xml_declaration=True, encoding='utf-8', standalone="yes", pretty_print=True)
            with open(os.path.join(os.path.abspath('.'), 'outputs', '%s.obxml' % obxml), 'wb') as output:
                output.write(obx)

        if rawobs:
            obx = etree.tostring(rootO, xml_declaration=True, encoding='utf-8', standalone="yes", pretty_print=True)
            with open(os.path.join(os.path.abspath('.'), 'outputs', '%s.rawobs' % rawobs), 'wb') as output:
                output.write(obx)

        click.echo('Done!')
    else:
        click.echo('No results.')


@cli.command()
@click.option('--instrument', type=click.Choice(instruments, case_sensitive=False))
@click.option('--target_name', default=None)
@click.option('--target_ra', default=None)
@click.option('--target_dec', default=None)
@click.option('--prog_id', default=None)
def oidb_observed_target(instrument, target_name, target_ra, target_dec, prog_id):

    # Build the query
    query_filters = []

    if instrument:
        query_filters.append("instrument_name = '%s'" % instrument)

    if target_name:
        query_filters.append("target_name = '%s'" % target_name)

    if target_ra:
        #TODO
        pass

    if target_dec:
        #TODO
        pass

    if prog_id:
        query_filters.append("progid = '%s'" % prog_id)

    oidb = vo.dal.TAPService("http://oidb-beta.jmmc.fr/tap")
    oidb_query = "SELECT ALL * FROM oidb"
    if query_filters:
        oidb_query += ' WHERE %s' % ' AND '.join(query_filters)

        oidb_result = oidb.search(oidb_query)

        if oidb_result:

            # Build dictionnary
            for result_row in oidb_result:

                row_data = {
                    'instrument': result_row['instrument_name'],
                    'prog_id': result_row['progid'],
                    'target': result_row['target_name'],
                    
                    'exposure': result_row['t_exptime'],
                    'mjd_min': result_row['t_min'],
                    'mjd_max': result_row['t_max'],
                }
                if row_data['mjd_min']:
                    row_data['isot_min'] = mjd_to_isot(row_data['mjd_min'])
                if row_data['mjd_max']:
                    row_data['isot_max'] = mjd_to_isot(row_data['mjd_max'])

                print(row_data)


## GET HEADER
########################################################################################################################
# See: https://raw.githubusercontent.com/astropy/astroquery/master/astroquery/eso/core.py

regex_header_id = re.compile('^([A-Z]+)\.(\d{4}-\d{2}-\d{2})T\d{2}:\d{2}:\d{2}(?:\.\d*)?$')

def _get_eso_header(header_id, parse_header=False, save_file=True, file_path=None):

    html_page = requests.get("http://archive.eso.org/hdr?DpId={0}".format(header_id))
    html_content_root = BeautifulSoup(html_page.content, 'html5lib')
    raw_header = html_content_root.select('pre')[0].text

    if save_file:
        m = regex_header_id.match(header_id)

        if m.group(1).startswith('AMB'):
            instrument = 'amber'
        elif m.group(1).startswith('GRA'):
            instrument = 'gravity'
        elif m.group(1).startswith('MAT'):
            instrument = 'matisse'
        elif m.group(1).startswith('MID'):
            instrument = 'midi'
        elif m.group(1).startswith('PIO'):
            instrument = 'pionier'


        if file_path is not None:
            output_path = file_path
        else:
            output_dir = os.path.join(os.path.abspath('.'), 'outputs', 'headers', instrument, m.group(2))
            _makedirs(output_dir)
            output_path = os.path.join(output_dir, '%s.txt' % header_id)

        with open(output_path, 'w', encoding="utf-8") as output:
            output.write(raw_header)

    if parse_header:
        return _parse_eso_header(raw_header)
    else:
        return raw_header


def _parse_eso_header(raw_header):
    header = {
        #'DP.ID': dp_id
    }
    for key_value in raw_header.split('\n'):
        if "=" in key_value:
            key, value = key_value.split('=', 1)
            key = key.strip()
            value = value.split('/', 1)[0].strip()
            if key[0:7] != "COMMENT":  # drop comments
                if value == "T":  # Convert boolean T to True
                    value = True
                elif value == "F":  # Convert boolean F to False
                    value = False
                # Convert to string, removing quotation marks
                elif value[0] == "'":
                    value = value[1:-1]
                elif "." in value:  # Convert to float
                    value = float(value)
                else:  # Convert to integer
                    value = int(value)
                header[key] = value
        elif key_value.startswith("END"):
            break

    return header


def _parse_eso_header_with_comments(raw_header, convert_bool=False, convert_float=False, convert_int=False):
    header = []

    for key_value in raw_header.split('\n'):
        if "=" in key_value:

            # Extract keyword
            key, value_comment = key_value.split('=', 1)
            key = key.strip()

            # Extract value and comment
            value = value_comment.split('/', 1)[0].strip()
            comment = value_comment.split('/', 1)[1].strip()

            # Clean value
            if key[0:7] != "COMMENT":  # drop comments

                if value == "T":  # Convert boolean T to True
                    if convert_bool:
                        value = True
                elif value == "F":  # Convert boolean F to False
                    if convert_bool:
                        value = False

                # Convert to string, removing quotation marks
                elif value[0] == "'":
                    value = value[1:-1]
                elif "." in value:  # Convert to float
                    if convert_float:
                        value = float(value)
                else:  # Convert to integer
                    if convert_int:
                        value = int(value)

                # Save the row
                row = {
                    'keyword': key,
                    'value': value,
                    'comment': comment
                }
                header.append(row)

        elif key_value.startswith("END"):
            break

    return header


@cli.command()
@click.argument('header_id')
def eso_download_header(header_id):
    click.echo('Downloading header %s' % header_id)
    file_path = os.path.join('outputs', 'headers', '%s.txt' % header_id)
    _get_eso_header(header_id, file_path=file_path)
    click.echo("File saved as '%s'" % file_path)


@cli.command()
@click.option('--instrument', type=click.Choice(instruments, case_sensitive=False))
@click.option('--target_name', default=None)
@click.option('--prog_id', default=None)
@click.option('--night', default=None)
def eso_download_headers(instrument, target_name, prog_id, night):

    # Build the query
    query_filters = {
        'dp_cat': [
            'SCIENCE',
            #'CALIB',
        ],
        'dp_tech': 'INTERFEROMETRY',
    }

    if instrument:
        query_filters['instrument'] = instrument

    if target_name:
        query_filters['target'] = target_name

    if prog_id:
        query_filters['prog_id'] = prog_id

    if night:
        query_filters['night'] = night

    # Execute the query
    eso = Eso()
    eso.ROW_LIMIT = -1  # Return all results
    eso_result = eso.query_main(column_filters=query_filters)

    if eso_result:
        print('%i headers found!' % len(eso_result))
        #print(eso_result)
        header_ids = eso_result['Dataset ID']
        for header_id in header_ids:
            click.echo("Downloading header '%s'..." % header_id)
            #print(header_id)
            _get_eso_header(header_id)
        click.echo('Done!')
    else:
        click.echo('No results.')



@cli.command()
@click.argument('path', type=click.Path(exists=True))
@click.option('--with_headers', is_flag=True, help='With headers')
def eso_header_to_csv(path, with_headers):
    click.echo('Converting header %s to CSV' % path)

    # Read header
    raw_header = None
    with open(path, 'r') as input_file:
        raw_header = input_file.read()

    # Write CSV
    with open('%s.csv' % path, 'wb') as output_file:
        writer = csv.DictWriter(output_file, fieldnames=['keyword', 'value', 'comment'])
        if with_headers:
            writer.writeheader()
        writer.writerows(_parse_eso_header_with_comments(raw_header))

    click.echo('Done!')



## TOOL ENTRY POINT
########################################################################################################################
if __name__ == '__main__':
    cli()
