#!/bin/bash

echo "clean file $1"

FILE=$1

grep -v "HIERARCH ESO ACQ" $FILE \
    | grep -v "HIERARCH ESO DEL DLT" \
    | grep -v "HIERARCH ESO DET" \
    | grep -v "HIERARCH ESO FDDL" \
    | grep -v "HIERARCH ESO FT DISP" \
    | grep -v "HIERARCH ESO FT KAL" \
    | grep -v "HIERARCH ESO FT OPDC" \
    | grep -v "HIERARCH ESO INS ANLO" \
    | grep -v "HIERARCH ESO INS DET" \
    | grep -v "HIERARCH ESO INS DPOR" \
    | grep -v "HIERARCH ESO INS DROT" \
    | grep -v "HIERARCH ESO INS FAFT" \
    | grep -v "HIERARCH ESO INS FILT" \
    | grep -v "HIERARCH ESO INS HWPOFFSET" \
    | grep -v "HIERARCH ESO INS KMIRROROFFSET" \
    | grep -v "HIERARCH ESO INS LA" \
    | grep -v "HIERARCH ESO INS ML" \
    | grep -v "HIERARCH ESO INS MTF" \
    | grep -v "HIERARCH ESO INS OFFANG" \
    | grep -v "HIERARCH ESO INS OPTI" \
    | grep -v "HIERARCH ESO INS PMC" \
    | grep -v "HIERARCH ESO INS POS" \
    | grep -v "HIERARCH ESO INS ROTATIONOFFSET" \
    | grep -v "HIERARCH ESO INS SENS" \
    | grep -v "HIERARCH ESO INS SHUT" \
    | grep -v "HIERARCH ESO INS SLC" \
    | grep -v "HIERARCH ESO INS TIM" \
    | grep -v "HIERARCH ESO INS VG" \
    | grep -v "HIERARCH ESO ISS ARAL" \
    | grep -v "HIERARCH ESO ISS AT" \
    | grep -v "HIERARCH ESO ISS M" \
    | grep -v "HIERARCH ESO MET" \
    | grep -v "HIERARCH ESO OCS MET" \
    | grep -v "HIERARCH ESO OCS TTC" \
 > clean_$FILE

