[0;32mINFO[0m: List of accepted column_filters parameters. [astroquery.eso.core]
[0;32mINFO[0m: The presence of a column in the result table can be controlled if prefixed with a [ ] checkbox. [astroquery.eso.core]
[0;32mINFO[0m: The default columns in the result table are shown as already ticked: [x]. [astroquery.eso.core]

Target Information
------------------
    target: 
    resolver: simbad (SIMBAD name), ned (NED name), none (OBJECT as specified by the observer)
    coord_sys: eq (Equatorial (FK5)), gal (Galactic)
    coord1: 
    coord2: 
    box: 
    format: sexagesimal (Sexagesimal), decimal (Decimal)
[x] wdb_input_file: 

Observation and proposal parameters
-----------------------------------
[ ] night: 
    stime: 
    starttime: 00 (00 hrs [UT]), 01 (01 hrs [UT]), 02 (02 hrs [UT]), 03 (03 hrs [UT]), 04 (04 hrs [UT]), 05 (05 hrs [UT]), 06 (06 hrs [UT]), 07 (07 hrs [UT]), 08 (08 hrs [UT]), 09 (09 hrs [UT]), 10 (10 hrs [UT]), 11 (11 hrs [UT]), 12 (12 hrs [UT]), 13 (13 hrs [UT]), 14 (14 hrs [UT]), 15 (15 hrs [UT]), 16 (16 hrs [UT]), 17 (17 hrs [UT]), 18 (18 hrs [UT]), 19 (19 hrs [UT]), 20 (20 hrs [UT]), 21 (21 hrs [UT]), 22 (22 hrs [UT]), 23 (23 hrs [UT]), 24 (24 hrs [UT])
    etime: 
    endtime: 00 (00 hrs [UT]), 01 (01 hrs [UT]), 02 (02 hrs [UT]), 03 (03 hrs [UT]), 04 (04 hrs [UT]), 05 (05 hrs [UT]), 06 (06 hrs [UT]), 07 (07 hrs [UT]), 08 (08 hrs [UT]), 09 (09 hrs [UT]), 10 (10 hrs [UT]), 11 (11 hrs [UT]), 12 (12 hrs [UT]), 13 (13 hrs [UT]), 14 (14 hrs [UT]), 15 (15 hrs [UT]), 16 (16 hrs [UT]), 17 (17 hrs [UT]), 18 (18 hrs [UT]), 19 (19 hrs [UT]), 20 (20 hrs [UT]), 21 (21 hrs [UT]), 22 (22 hrs [UT]), 23 (23 hrs [UT]), 24 (24 hrs [UT])
[x] prog_id: 
[ ] prog_type: % (Any), 0 (Normal), 1 (GTO), 2 (DDT), 3 (ToO), 4 (Large), 5 (Short), 6 (Calibration)
[ ] obs_mode: % (All modes), s (Service), v (Visitor)
[ ] pi_coi: 
    pi_coi_name: PI_only (as PI only), none (as PI or CoI)
[ ] prog_title: 

Generic File Information
------------------------
[x] dp_id: 
[x] ob_id: 
[x] obs_targ_name: 
[x] dp_cat: % (Any), ACQUISITION (ACQUISITION), CALIB (CALIB), SCIENCE (SCIENCE)
[x] dp_type: % (Any), BIAS% or DARK% or FLAT% or FMTCHK% or WAVE%  (---------Calibration files:---------), BIAS% (BIAS), DARK% (DARK), FLAT% (FLAT), FMTCHK% (FMTCHK), WAVE% (WAVE), WAVE,CALIBRATION% (WAVE,CALIBRATION), WAVE,SPECTEMPL% (WAVE,SPECTEMPL), COARSE% or FRINGE_SEARCH,OBJECT% or SEARCH,OBJECT%  (--------Acquisition files:--------), COARSE,OBJECT% (COARSE,OBJECT), FRINGE_SEARCH,OBJECT% (FRINGE_SEARCH,OBJECT), SEARCH,OBJECT% (SEARCH,OBJECT), FRINGE_TRACK% or TRACK% or PHOTOMETRY% or  (----Interferometry and Photometry files:----), FRINGE_TRACK% (FRINGE_TRACK), FRINGE_TRACK,OBJECT,FIELD (FRINGE_TRACK,OBJECT,FIELD), FRINGE_TRACK,OBJECT,FOURIER (FRINGE_TRACK,OBJECT,FOURIER), FRINGE_TRACK,OBJECT,FOURIER,SC% (FRINGE_TRACK,OBJECT,FOURIER,SCI_PHOT), TRACK,OBJECT,DISPERSED% (TRACK,OBJECT,DISPERSED), TRACK,OBJECT,DISPERSED,SCIPHOT% (TRACK,OBJECT,DISPERSED,SCIPHOT), PHOTOMETRY,OBJECT% (PHOTOMETRY,OBJECT), PHOTOMETRY,OBJECT,SCI% (PHOTOMETRY,OBJECT,SCIPHOT)
    dp_type_user: 
[x] dp_tech: % (Any), ALIGNMENT% or DETECTOR% or IMAGE%=IMAGE or SPECTRUM% (-----Calibration files:-----), ALIGNMENT,REFPIXELS% (ALIGNMENT,REFPIXELS), DETECTOR,LINEARITY% (DETECTOR,LINEARITY), DETECTOR,NOISE% (DETECTOR,NOISE), IMAGE% (IMAGE), SPECTRUM% (SPECTRUM), GENERIC% or SPECTRO,TRANSMISSION% (-----Maintenance files:-----), GENERIC% (GENERIC), SPECTRO,TRANSMISSION% (SPECTRO,TRANSMISSION), IMAGE,WINDOW% (-----Acquisition files-----), IMAGE,WINDOW (IMAGE,WINDOW), IMAGE,WINDOW,CHOPNOD% or INTERFEROMETRY% or OTHER% (--Interferometric and photometric files:--), IMAGE,WINDOW,CHOPNOD (IMAGE,WINDOW,CHOPNOD), INTERFEROMETRY% (INTERFEROMETRY), OTHER% (OTHER)
    dp_tech_user: 
[x] ins_mode: % (Any), AUTOTEST% (AUTOTEST), STARINTF% (STARINTF)
[ ] dit: 
[ ] ndit: 
[ ] tpl_id: % (Any), MIDI_autotest_tec_detlin% (autotest_tec_detlin), MIDI_autotest_tec_detron% (autotest_tec_detron), MIDI_autotest_tec_dsptrn% (autotest_tec_dsptrn), MIDI_autotest_tec_generic% (autotest_tec_generic), MIDI_autotest_tec_piezo% (autotest_tec_piezo), MIDI_autotest_tec_refpix% (autotest_tec_refpix), MIDI_autotest_tec_wavecal% (autotest_tec_wavecal), MIDI_starintf_acq% (starintf_acq), MIDI_starintf_obs% (-----Any starintf_obs-----), MIDI_starintf_obs_field% (starintf_obs_field), MIDI_starintf_obs_fringe_cal% (starintf_obs_fringe_cal), MIDI_starintf_obs_fringe_sci% (starintf_obs_fringe_sci), MIDI_starintf_obs_fringe% (starintf_obs_fringe), MIDI_starintf_obs_sciphot% (starintf_obs_sciphot)
[ ] tpl_start: 
[ ] tpl_nexp: 
[ ] tpl_expno: 

Instrument Specific Information
-------------------------------
[ ] ins_cam_name: % (Any), FIELD% (FIELD), OPEN% (OPEN), PUPIL% (PUPIL), SPECTRAL% (SPECTRAL)
[ ] ins_filt_name: % (Any), SiC% (SiC), N8.7% (N8.7), N11.3% (N11.3), Nband% (Nband), ND1-N-BAND% (ND1-N-BAND), [SIV]% ([SIV]), [NeII]% ([NeII]), [ArIII]% ([ArIII]), WL-CALIB_1% (WL-CALIB_1), WL-CALIB_2% (WL-CALIB_2), OPEN% (OPEN), CLOSED% (CLOSED)
[ ] ins_gris_name: % (Any), GRISM% (GRISM), PRISM% (PRISM), OPEN% (OPEN), OPEN1% (OPEN1), AUTO_COLL% (AUTO_COLL)
[ ] ins_opt1_name: % (Any), SCI_PHOT% (SCI_PHOT), HIGH_SENS% (HIGH_SENS), OPEN% (OPEN), OPEN1% (OPEN1), OPEN2% (OPEN2), AUTO_COLL% (AUTO_COLL)
[ ] ins_shut_name: % (Any), AOPEN% (AOPEN), BOPEN% (BOPEN), ABOPEN% (ABOPEN), CLOSED% (CLOSED)
[ ] ins_slit_name: % (Any), FullField% (FullField), S_0.4C% (S_0.4C), SLIT_0.05% (SLIT_0.05), SLIT_0.1% (SLIT_0.1), SLIT_0.2% (SLIT_0.2), T_0.07C% (T_0.07C), T_0.11C% (T_0.11C), T_0.17C% (T_0.17C), OPEN% (OPEN), FIBER% (FIBER)
[ ] ins_piez_posnum: 
[ ] iss_conf_ntel: 
[ ] iss_conf_station1: % (Any), U1% (UT1), U2% (UT2), U3% (UT3), U4% (UT4), A0% (A0), A1% (A1), B0% (B0), B1% (B1), B2% (B2), B3% (B3), B4% (B4), B5% (B5), C1% (C1), C2% (C2), C3% (C3), D0% (D0), D1% (D1), D2% (D2), E0% (E0), G0% (G0), G1% (G1), G2% (G2), H0% (H0), I1% (I1), J1% (J1), J2% (J2), J3% (J3), J4% (J4), J5% (J5), J6% (J6), K0% (K0), L0% (L0), M0% (M0)
[ ] iss_conf_station2: % (Any), U1% (UT1), U2% (UT2), U3% (UT3), U4% (UT4), A0% (A0), A1% (A1), B0% (B0), B1% (B1), B2% (B2), B3% (B3), B4% (B4), B5% (B5), C1% (C1), C2% (C2), C3% (C3), D0% (D0), D1% (D1), D2% (D2), E0% (E0), G0% (G0), G1% (G1), G2% (G2), H0% (H0), I1% (I1), J1% (J1), J2% (J2), J3% (J3), J4% (J4), J5% (J5), J6% (J6), K0% (K0), L0% (L0), M0% (M0)
[ ] iss_chop_st: 
[ ] iss_chop_freq: 
[ ] iss_chop_throw: 
[ ] del_ft_sensor: 
[ ] del_ft_status: 

Ambient Parameters
------------------
[x] fwhm_avg: 
[ ] airmass_range: 
[ ] night_flag: % (Any), 0 (Night), 1 (Twilight), 2 (Daytime)
[ ] moon_illu: 

Result set
----------
    order:  (nothing (faster)), dp_id (Observation Time), dp_cat (DPR.CATG), dp_tech (DPR.TECH), tpl_start (TPL.START), ob_id asc (OB.ID (ascending)), ob_id desc (OB.ID (descending)), period asc,prog_id asc (Period and Run ID (earliest first)), period desc,prog_id desc (Period and Run ID (latest first))
