SSHADE README
==================

Getting Started
---------------

Init a Python virtualenv

- cd <directory containing this file>

- virtualenv .env


Activate the virtualenv

- cd <directory containing this file>

- source .env/bin/activate

Now, we're assuming you are inside <directory containing this file> in the activated virtualenv!



Install dependecies

- pip install --upgrade pip -r requirements.txt


Usage
-----

- python eso-cli.py <COMMAND> [OPTIONS OR PARAMETERS]

Note:
- Without the <COMMAND> argument, the tool will list the available commands


Links
-----

- https://astroquery.readthedocs.io/en/latest/
- https://astroquery.readthedocs.io/en/latest/eso/eso.html

- https://docs.astropy.org/en/stable/
- https://pyvo.readthedocs.io/en/latest/

- https://lxml.de/
- https://click.palletsprojects.com/
- https://arrow.readthedocs.io/en/latest/
- https://capturer.readthedocs.io/en/latest/
